﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using MindMachineForWin8.Common;
using MindMachineForWin8.Model;
using MindMachineForWin8.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace MindMachineForWin8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class FlashingPage : MindMachineForWin8.Common.LayoutAwarePage
    {
        public FlashingPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            var fpp = navigationParameter as FlashingPageParameters ?? 
                new FlashingPageParameters()
                    {
                        MindProgram = KnownMindPrograms.Relaxation.MindTopics[0].CreateMindProgram(20),
                        Music = "A walk on a beach - Alban Lepsy",
                        TopicName = "Sample Relaxation"
                    };
            var vm = new FlashingViewModel();
            vm.Activate(fpp, pageState);
            this.DataContext = vm;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void BackgroundMusic_MediaEnded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        	BackgroundMusic.Position = TimeSpan.FromSeconds(0); 
			BackgroundMusic.Play();
        }
    }

    [DataContract]
    public class FlashingPageParameters
    {
        [DataMember]
        public String Music { get; set; }
        [DataMember]
        public String TopicName { get; set; }
        [DataMember]
        public MindProgram MindProgram { get; set; }
    }
}
