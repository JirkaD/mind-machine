﻿using MindMachineForWin8.Data;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Grouped Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234231

namespace MindMachineForWin8
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class GroupedItemsPage : MindMachineForWin8.Common.LayoutAwarePage
    {
        public GroupedItemsPage()
        {
            this.InitializeComponent();
            DataTransferManager.GetForCurrentView().DataRequested += GroupedItemsPage_DataRequested;

        }

        async void GroupedItemsPage_DataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            args.Request.Data.Properties.Title = "Mind Machine for Windows 8";
            args.Request.Data.Properties.Description = "Mind Machine application uses flashing light to alter the brainwave frequency of the user. "+
            "It can induce deep states of relaxation and concentration. ";
            args.Request.Data.SetText("Hello World!");
            args.Request.Data.SetBitmap(RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/original.jpg")));
            //args.Request.Data.SetUri(new Uri("ms-appx:///Assets/original.jpg"));
           
    /*            DataRequest request = args.Request;
    request.Data.Properties.Title = "Mind Machine for Windows 8";
    request.Data.Properties.Description = "App for enhancing brainwaves";

    // Because we are making async calls in the DataRequested event handler,
    //  we need to get the deferral first.
    DataRequestDeferral deferral = request.GetDeferral();   

    // Make sure we always call Complete on the deferral.
    try
    {
        StorageFile thumbnailFile = 
            await Package.Current.InstalledLocation.GetFileAsync("Assets\\original.jpg");
        request.Data.Properties.Thumbnail = 
            RandomAccessStreamReference.CreateFromFile(thumbnailFile);
        //StorageFile imageFile = 
        //    await Package.Current.InstalledLocation.GetFileAsync("Assets\\original.jpg");
        request.Data.SetText("Hello world!");
       // request.Data.SetBitmap(RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/usage.png")));
    }
    finally
    {
        deferral.Complete();
    }*/
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            DataTransferManager.GetForCurrentView().DataRequested -= GroupedItemsPage_DataRequested;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            var sampleDataGroups = SampleDataSource.GetGroups((String)navigationParameter);
            this.DefaultViewModel["Groups"] = sampleDataGroups;
        }

        /// <summary>
        /// Invoked when a group header is clicked.
        /// </summary>
        /// <param name="sender">The Button used as a group header for the selected group.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>
        void Header_Click(object sender, RoutedEventArgs e)
        {
            // Determine what group the Button instance represents
            var group = (sender as FrameworkElement).DataContext;

            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            //this.Frame.Navigate(typeof(GroupDetailPage), ((SampleDataGroup)group).UniqueId);

         /*   this.Frame.Navigate(typeof(GroupDetailPage), ((SampleDataGroup)group).Items[0].UniqueId);
            var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;*/
       //     this.Frame.Navigate(typeof(ItemDetailPage), ((SampleDataGroup)group).Items[0].UniqueId);
        }

        /// <summary>
        /// Invoked when an item within a group is clicked.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is snapped)
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
            this.Frame.Navigate(typeof(ItemDetailPage), itemId);
        }
    }
}
