﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;

namespace MindMachineForWin8.Model
{
    [DataContract]
    public struct Settings
    {
        [DataMember]
        public bool IsVisualStimulation;
        [DataMember]
        public bool IsAudioStimulation;
        [DataMember]
        public Color LightColor;
        [DataMember]
        public Tone StimulationTone;
        [DataMember]
        public int SelectedMusic;
    }
}
