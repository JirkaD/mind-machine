﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{
    [DataContract]
    public struct Tone
    {
        [DataMember]
        public string Name;
        [DataMember]
        public double Frequency;

        public override string ToString()
        {
            return Name;
        }

        public static Tone[] BasicTones = 
        {
            new Tone{Name = "C1", Frequency = 32.70},                                  
            new Tone{Name = "D1", Frequency = 36.71},                                  
            new Tone{Name = "E1", Frequency = 41.20},                                  
            new Tone{Name = "F1", Frequency = 43.65},                                  
            new Tone{Name = "G1", Frequency = 51.91},                                  
            new Tone{Name = "A1", Frequency = 55.00},                                  
            new Tone{Name = "B1", Frequency = 61.74},                                  
            new Tone{Name = "C2", Frequency = 65.41},                                  
            new Tone{Name = "D2", Frequency = 73.42},                                  
            new Tone{Name = "E2", Frequency = 82.41},                                  
            new Tone{Name = "F2", Frequency = 87.31},                                  
            new Tone{Name = "G2", Frequency = 98.00},                                  
            new Tone{Name = "A2", Frequency = 110.00},                                  
            new Tone{Name = "B2", Frequency = 123.47},                                  
         /*   new Tone{Name = "C3", Frequency = 130.81},                                  
            new Tone{Name = "D3", Frequency = 146.83},                                  
            new Tone{Name = "E3", Frequency = 164.81},                                  
            new Tone{Name = "F3", Frequency = 174.61},                                  
            new Tone{Name = "G3", Frequency = 196.00},                                  
            new Tone{Name = "A3", Frequency = 220.00},                                  
            new Tone{Name = "B3", Frequency = 246.94},                                  
            new Tone{Name = "C4", Frequency = 261.63},      */                            
        };
    }
}
