﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{
    [DataContract]
    public class MindTopic
    {
        [DataMember]
        public readonly String Name;
        [DataMember]
        public int MinLenghtInMinutes { get; set; }
        [DataMember]
        public int MaxLenghtInMinutes { get; set; }

        [DataMember]
        public readonly MindProgram[] CoreMindPrograms;

        [DataMember]
        public string ImageName;

        public string TileImage;

        public MindTopic(String name, FrequencyPoint[][] graphs, string tileImageName=null)
            : this(name, (from graph in graphs select MindProgram.FromGraph(graph)).ToArray(), tileImageName)
        {
        }

        public MindTopic(String name, MindProgram[] coreMindPrograms, string tileImageName=null)
        {
            TileImage = tileImageName;

            Name = name;

            CoreMindPrograms = coreMindPrograms;
            var maxProgramLength = coreMindPrograms.Select(mindProgram => mindProgram.Lenght.TotalMinutes).Max();
            var minProgramLength = coreMindPrograms.Select(mindProgram => mindProgram.Lenght.TotalMinutes).Min();
            MinLenghtInMinutes = (int)(minProgramLength * 0.75);
            MaxLenghtInMinutes = (int)(maxProgramLength * 1.5);
        }

        public MindProgram CreateMindProgram(int lengthInMinutes)
        {
            var closestProgram = CoreMindPrograms[0];
            double closestDistance = Math.Abs(CoreMindPrograms[0].Lenght.TotalMinutes - lengthInMinutes);
            foreach (var mindProgram in CoreMindPrograms)
            {
                double distance = Math.Abs(mindProgram.Lenght.TotalMinutes - lengthInMinutes);
                if (distance < closestDistance)
                {
                    closestProgram = mindProgram;
                    closestDistance = distance;
                }
            }
            return MindProgram.Scale(closestProgram, TimeSpan.FromMinutes(lengthInMinutes));
        }
    }
}

