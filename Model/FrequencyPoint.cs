﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{
        public struct FrequencyPoint
        {
            public double Time;
            public double Frequency;
        }
}
