﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{
    [DataContract]
    public class MindCathegory
    {
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public MindTopic[] MindTopics { get; set; }
        [DataMember]
        public bool IsFullVersionOnly { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string ImageName;


        public MindCathegory(string name, MindTopic[] mindTopics, String description, String imageName, bool isFullVersionOnly = false)
        {
            Name = name;
            MindTopics = mindTopics;
            IsFullVersionOnly = isFullVersionOnly;
            Description = description;
            ImageName = imageName;

            foreach (var mindTopic in MindTopics)
            {
                mindTopic.ImageName = ImageName;
            }
        }
    }
}
