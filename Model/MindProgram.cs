﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{

    [DataContract]
    public class MindProgram
    {
        [DataMember]
        public FrequencyPoint[] Graph;

        public MindProgram(FrequencyPoint[] graph)
        {
            CheckGraph(graph);
            Graph = graph;
        }

        private void CheckGraph(FrequencyPoint[] graph)
        {
            if (graph.Length < 2)
                throw new ArgumentException("Too few frequency points.");
            if (graph[0].Time != 0)
                throw new ArgumentException("Starting frequency point is missing.");
            for (int i = 0; i < graph.Length - 1; i++)
            {
                if (graph[i].Time >= graph[i + 1].Time)
                    throw new ArgumentException("Frequency points are out of order.");
            }
            for (int i = 0; i < graph.Length; i++)
            {
                if (graph[i].Frequency <= 0)
                    throw new ArgumentException("Frequency must be positive.");
            }
        }

        public TimeSpan Lenght
        {
            get { return TimeSpan.FromMinutes(Graph[Graph.Length - 1].Time); }
        }

        public double GetFrequency(TimeSpan time)
        {
            double timeInMinutes = time.TotalMinutes;
            for (int i = 0; i < Graph.Length - 1; i++)
            {
                if (Graph[i].Time <= timeInMinutes && Graph[i + 1].Time >= timeInMinutes)
                {
                    double frequency1 = Graph[i].Frequency;
                    double weight1 = (timeInMinutes - Graph[i].Time) / (Graph[i + 1].Time + Graph[i].Time);
                    double frequency2 = Graph[i + 1].Frequency;
                    double weight2 = 1 - weight1;
                    double frequency = weight1 * frequency1 + weight2 * frequency2;
                    return frequency;
                }
            }
            return Graph[Graph.Length - 1].Frequency;
        }


        public static MindProgram FromGraph(FrequencyPoint[] graph)
        {
            return new MindProgram(graph);
        }

        public static MindProgram Scale(MindProgram mindProgram, TimeSpan targetLenght)
        {
            var scaledFrequencyPoints = (from point in mindProgram.Graph
                                         select
                                             new FrequencyPoint
                                             {
                                                 Frequency = point.Frequency,
                                                 Time = point.Time * targetLenght.TotalMinutes / mindProgram.Lenght.TotalMinutes
                                             }).ToArray();
            return new MindProgram(scaledFrequencyPoints);
        }

        public static MindProgram CreateConstant(double frequency, TimeSpan length)
        {
            return FromGraph(new[] { new FrequencyPoint { Frequency = frequency }, 
                new FrequencyPoint { Frequency = frequency, Time = length.TotalMinutes } });
        }

    }

}

