﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindMachineForWin8.Model
{
    public class KnownMindPrograms
    {
        #region RELAXATION
        private static readonly FrequencyPoint[] Relaxation15 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 6, Time = 5},
                new FrequencyPoint{Frequency = 9, Time = 8},
                new FrequencyPoint{Frequency = 6, Time = 10},
                new FrequencyPoint{Frequency = 14.5, Time = 15}
            };
        private static readonly FrequencyPoint[] Relaxation25 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 6, Time = 5},
                new FrequencyPoint{Frequency = 9, Time = 7.5},
                new FrequencyPoint{Frequency = 6, Time = 10},
                new FrequencyPoint{Frequency = 9, Time = 12.5},
                new FrequencyPoint{Frequency = 6, Time = 15},
                new FrequencyPoint{Frequency = 9, Time = 17.5},
                new FrequencyPoint{Frequency = 6, Time = 20},
                new FrequencyPoint{Frequency = 14.5, Time = 25}
            };
        private static readonly FrequencyPoint[] Relaxation35 =
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 6, Time = 5},
                new FrequencyPoint{Frequency = 9, Time = 7.5},
                new FrequencyPoint{Frequency = 6, Time = 10},
                new FrequencyPoint{Frequency = 9, Time = 12.5},
                new FrequencyPoint{Frequency = 6, Time = 15},
                new FrequencyPoint{Frequency = 9, Time = 17.5},
                new FrequencyPoint{Frequency = 6, Time = 20},
                new FrequencyPoint{Frequency = 9, Time = 22.5},
                new FrequencyPoint{Frequency = 6, Time = 25},
                new FrequencyPoint{Frequency = 9, Time = 27.5},
                new FrequencyPoint{Frequency = 6, Time = 30},
                new FrequencyPoint{Frequency = 14.5, Time = 35}
            };
        private static readonly FrequencyPoint[] Relaxation45 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 6, Time = 5},
                new FrequencyPoint{Frequency = 9, Time = 7.5},
                new FrequencyPoint{Frequency = 6, Time = 10},
                new FrequencyPoint{Frequency = 9, Time = 12.5},
                new FrequencyPoint{Frequency = 6, Time = 15},
                new FrequencyPoint{Frequency = 9, Time = 17.5},
                new FrequencyPoint{Frequency = 6, Time = 20},
                new FrequencyPoint{Frequency = 9, Time = 22.5},
                new FrequencyPoint{Frequency = 6, Time = 25},
                new FrequencyPoint{Frequency = 9, Time = 27.5},
                new FrequencyPoint{Frequency = 6, Time = 30},
                new FrequencyPoint{Frequency = 9, Time = 32.25},
                new FrequencyPoint{Frequency = 6, Time = 35},
                new FrequencyPoint{Frequency = 9, Time = 37.5},
                new FrequencyPoint{Frequency = 6, Time = 40},
                new FrequencyPoint{Frequency = 14.5, Time = 45}
            };
        private static readonly FrequencyPoint[] DeepRelaxation = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 12, Time = 2},
                new FrequencyPoint{Frequency = 7, Time = 3},
                new FrequencyPoint{Frequency = 7, Time = 4},
                new FrequencyPoint{Frequency = 3, Time = 5},
                new FrequencyPoint{Frequency = 3, Time = 9},
                new FrequencyPoint{Frequency = 4, Time = 10},
                new FrequencyPoint{Frequency = 4, Time = 19},
                new FrequencyPoint{Frequency = 8, Time = 20},
                new FrequencyPoint{Frequency = 8, Time = 24},
                new FrequencyPoint{Frequency = 11.5, Time = 25},
                new FrequencyPoint{Frequency = 12, Time = 30}

            };
        private static readonly FrequencyPoint[] MinorStressRemoval = 
            {
                new FrequencyPoint{Frequency = 15, Time = 0},
                new FrequencyPoint{Frequency = 15, Time = 3},
                new FrequencyPoint{Frequency = 10.5, Time = 5},
                new FrequencyPoint{Frequency = 10, Time = 7},
                new FrequencyPoint{Frequency = 10, Time = 15},
                new FrequencyPoint{Frequency = 7, Time = 20},
                new FrequencyPoint{Frequency = 7, Time = 29},
                new FrequencyPoint{Frequency = 17, Time = 30}
            };
        public static readonly FrequencyPoint[] MajorStressRemoval = 
            {
                new FrequencyPoint{Frequency = 25, Time = 0},
                new FrequencyPoint{Frequency = 25, Time = 4},
                new FrequencyPoint{Frequency = 14, Time = 5},
                new FrequencyPoint{Frequency = 8, Time = 8},
                new FrequencyPoint{Frequency = 8, Time = 18},
                new FrequencyPoint{Frequency = 5, Time = 19},
                new FrequencyPoint{Frequency = 5, Time = 29},
                new FrequencyPoint{Frequency = 14, Time = 30}
            };
        private static readonly FrequencyPoint[] TakeANap = 
            {
                new FrequencyPoint{Frequency = 15, Time = 0},
                new FrequencyPoint{Frequency = 15, Time = 10},
                new FrequencyPoint{Frequency = 3, Time = 13},
                new FrequencyPoint{Frequency = 5, Time = 17},
                new FrequencyPoint{Frequency = 3, Time = 23},
                new FrequencyPoint{Frequency = 5, Time = 27},
                new FrequencyPoint{Frequency = 3, Time = 33},
                new FrequencyPoint{Frequency = 5, Time = 37},
                new FrequencyPoint{Frequency = 3, Time = 43},
                new FrequencyPoint{Frequency = 5, Time = 47},
                new FrequencyPoint{Frequency = 3, Time = 53},
                new FrequencyPoint{Frequency = 5, Time = 57},
                new FrequencyPoint{Frequency = 3, Time = 63},
                new FrequencyPoint{Frequency = 3, Time = 67},
                new FrequencyPoint{Frequency = 27, Time = 68}
            };
        #endregion RELAXATION

        public static readonly MindCathegory Relaxation = new MindCathegory("Relaxation", new[]
        {
            new MindTopic("Classic relaxation", new[] { Relaxation15, Relaxation25, Relaxation35, Relaxation45 }, "Images/relaxation1.jpg"),
            new MindTopic("Deep relaxation", new[] { DeepRelaxation }, "Images/relaxation2.jpg"),
            new MindTopic("Minor stress removal", new[] { MinorStressRemoval }),
            new MindTopic("Major stress removal", new[] { MajorStressRemoval }),
            new MindTopic("Take a nap", new[] { TakeANap })
        },
        "Relaxation programs will help you have a rest during the day or to remove some stress.", "Assets/Grapes.jpg");

        #region MEDITATION
        private static readonly FrequencyPoint[] HealingMeditation = 
            {
                new FrequencyPoint{Frequency = 20, Time = 0},
                new FrequencyPoint{Frequency = 1, Time = 0.5},
                new FrequencyPoint{Frequency = 15, Time = 1},
                new FrequencyPoint{Frequency = 1, Time = 1.5},
                new FrequencyPoint{Frequency = 10, Time = 2},
                new FrequencyPoint{Frequency = 3, Time = 2.5},
                new FrequencyPoint{Frequency = 7, Time = 3},
                new FrequencyPoint{Frequency = 5, Time = 7},
                new FrequencyPoint{Frequency = 3, Time = 17},
                new FrequencyPoint{Frequency = 3, Time = 22},
                new FrequencyPoint{Frequency = 20, Time = 30}
            };
        private static readonly FrequencyPoint[] ShortMeditation = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 10.5, Time = 1.2},
                new FrequencyPoint{Frequency = 3, Time = 1.4},
                new FrequencyPoint{Frequency = 5, Time = 1.5},
                new FrequencyPoint{Frequency = 5, Time = 5}
            };
        private static readonly FrequencyPoint[] DeepMeditation = 
            {
                new FrequencyPoint{Frequency = 18, Time = 0},
                new FrequencyPoint{Frequency = 2, Time = 10},
                new FrequencyPoint{Frequency = 4, Time = 15},
                new FrequencyPoint{Frequency = 2, Time = 20},
                new FrequencyPoint{Frequency = 4, Time = 30},
                new FrequencyPoint{Frequency = 2, Time = 38},
                new FrequencyPoint{Frequency = 20, Time = 40}
            };
        private static readonly FrequencyPoint[] ProgressiveMeditation = 
            {
                new FrequencyPoint{Frequency = 24, Time = 0},
                new FrequencyPoint{Frequency = 11, Time = 2.5},
                new FrequencyPoint{Frequency = 11, Time = 4},
                new FrequencyPoint{Frequency = 9, Time = 5},
                new FrequencyPoint{Frequency = 9, Time = 7},
                new FrequencyPoint{Frequency = 7, Time = 8},
                new FrequencyPoint{Frequency = 7, Time = 10},
                new FrequencyPoint{Frequency = 5, Time = 11},
                new FrequencyPoint{Frequency = 5, Time = 15},
                new FrequencyPoint{Frequency = 3, Time = 16},
                new FrequencyPoint{Frequency = 3, Time = 20},
                new FrequencyPoint{Frequency = 1, Time = 21},
                new FrequencyPoint{Frequency = 1, Time = 27},
                new FrequencyPoint{Frequency = 15, Time = 28},
                new FrequencyPoint{Frequency = 16, Time = 30}
            };
        private static readonly FrequencyPoint[] DeltaMeditation = 
            {
                new FrequencyPoint{Frequency = 15, Time = 0},
                new FrequencyPoint{Frequency = 2, Time = 0.5},
                new FrequencyPoint{Frequency = 15, Time = 1},
                new FrequencyPoint{Frequency = 2, Time = 1.5},
                new FrequencyPoint{Frequency = 15, Time = 2},
                new FrequencyPoint{Frequency = 2, Time = 2.5},
                new FrequencyPoint{Frequency = 8, Time = 3},
                new FrequencyPoint{Frequency = 8, Time = 3.5},
                new FrequencyPoint{Frequency = 6, Time = 4},
                new FrequencyPoint{Frequency = 2, Time = 22},
                new FrequencyPoint{Frequency = 2, Time = 28},
                new FrequencyPoint{Frequency = 24, Time = 30}
            };
        #endregion MEDITATION

        public static readonly MindCathegory Meditation = new MindCathegory("Meditation", new[]
        {
            new MindTopic("Healing meditation", new[] { HealingMeditation }, "Images/meditation1.jpg"),
            new MindTopic("Short meditation", new[] { ShortMeditation }, "Images/meditation2.jpg"),
            new MindTopic("Deep meditation", new[] { DeepMeditation }),
            new MindTopic("Progressive meditation", new[] { ProgressiveMeditation }),
            new MindTopic("Delta meditation", new[] { DeltaMeditation })
        },
        "Meditation programs will help you tune your brain into the meditation state.", "Assets/Meditation.jpg");

        #region BEFORE_SLEEP
        private static readonly FrequencyPoint[] GoingToSleep15 = 
            {
                new FrequencyPoint{Frequency = 9, Time = 0},         
                new FrequencyPoint{Frequency = 3, Time = 10},         
                new FrequencyPoint{Frequency = 5.2, Time = 13.5},         
                new FrequencyPoint{Frequency = 2, Time = 15}
            };

        private static readonly FrequencyPoint[] GoingToSleep25 = 
            {
                new FrequencyPoint{Frequency = 9, Time = 0},         
                new FrequencyPoint{Frequency = 3, Time = 8},         
                new FrequencyPoint{Frequency = 6, Time = 12.5},         
                new FrequencyPoint{Frequency = 3, Time = 17},             
                new FrequencyPoint{Frequency = 6, Time = 20},         
                new FrequencyPoint{Frequency = 1, Time = 25}
            };
        private static readonly FrequencyPoint[] GoingToSleep35 = 
            {
                new FrequencyPoint{Frequency = 10, Time = 0},         
                new FrequencyPoint{Frequency = 2, Time = 12},         
                new FrequencyPoint{Frequency = 7.5, Time = 16},         
                new FrequencyPoint{Frequency = 2, Time = 23.5},         
                new FrequencyPoint{Frequency = 7, Time = 27},         
                new FrequencyPoint{Frequency = 2.5, Time = 34},         
                new FrequencyPoint{Frequency = 1, Time = 35}
            };
        private static readonly FrequencyPoint[] GoingToSleep45 = 
            {
                new FrequencyPoint{Frequency = 11, Time = 0},         
                new FrequencyPoint{Frequency = 2, Time = 15},         
                new FrequencyPoint{Frequency = 7.5, Time = 20},         
                new FrequencyPoint{Frequency = 1.7, Time = 30},         
                new FrequencyPoint{Frequency = 6, Time = 34},         
                new FrequencyPoint{Frequency = 1.7, Time = 39},         
                new FrequencyPoint{Frequency = 5, Time = 44},         
                new FrequencyPoint{Frequency = 1, Time = 45}
            };
        private static readonly FrequencyPoint[] DeepSleep20 = 
            {
                new FrequencyPoint{Frequency = 14, Time = 0},         
                new FrequencyPoint{Frequency = 14, Time = 1},         
                new FrequencyPoint{Frequency = 4, Time = 7},         
                new FrequencyPoint{Frequency = 2, Time = 20}
            };
        private static readonly FrequencyPoint[] FallingAsleep5 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 1},         
                new FrequencyPoint{Frequency = 4, Time = 5}
            };
        private static readonly FrequencyPoint[] FallingAsleep10 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 1},         
                new FrequencyPoint{Frequency = 4, Time = 10}
            };
        private static readonly FrequencyPoint[] FallingAsleep15 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 1},         
                new FrequencyPoint{Frequency = 4, Time = 15}
            };
        private static readonly FrequencyPoint[] FallingAsleep20 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 1},         
                new FrequencyPoint{Frequency = 4, Time = 20}
            };
        private static readonly FrequencyPoint[] FallingAsleep30 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 1},         
                new FrequencyPoint{Frequency = 5.5, Time = 20},         
                new FrequencyPoint{Frequency = 4, Time = 30}
            };
        private static readonly FrequencyPoint[] SweetDreams30 = 
            {
                new FrequencyPoint{Frequency = 14, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 2.5},         
                new FrequencyPoint{Frequency = 12, Time = 4},         
                new FrequencyPoint{Frequency = 11, Time = 5},         
                new FrequencyPoint{Frequency = 10, Time = 6.5},         
                new FrequencyPoint{Frequency = 10, Time = 8.5},         
                new FrequencyPoint{Frequency = 6, Time = 10},         
                new FrequencyPoint{Frequency = 6, Time = 15},         
                new FrequencyPoint{Frequency = 4, Time = 17},         
                new FrequencyPoint{Frequency = 4, Time = 20},         
                new FrequencyPoint{Frequency = 2, Time = 21},         
                new FrequencyPoint{Frequency = 2, Time = 24},         
                new FrequencyPoint{Frequency = 1, Time = 25},         
                new FrequencyPoint{Frequency = 1, Time = 30}
            };
        private static readonly FrequencyPoint[] SleepingProblems60 = 
            {
                new FrequencyPoint{Frequency = 11, Time = 0},         
                new FrequencyPoint{Frequency = 14, Time = 2},         
                new FrequencyPoint{Frequency = 5, Time = 4},         
                new FrequencyPoint{Frequency = 14, Time = 6},         
                new FrequencyPoint{Frequency = 5, Time = 8},         
                new FrequencyPoint{Frequency = 8, Time = 9},         
                new FrequencyPoint{Frequency = 8, Time = 12},         
                new FrequencyPoint{Frequency = 6, Time = 14},         
                new FrequencyPoint{Frequency = 6, Time = 16},         
                new FrequencyPoint{Frequency = 1, Time = 19},         
                new FrequencyPoint{Frequency = 1, Time = 60}
            };
        #endregion BEFORE_SLEEP

        public static readonly MindCathegory BeforeSleep = new MindCathegory("Before sleep", new[]
        {
            new MindTopic("Going to sleep", new[] { GoingToSleep15, GoingToSleep25, GoingToSleep35, GoingToSleep45 }, "Images/sleep1.jpg"), 
            new MindTopic("Deep sleep", new[] { DeepSleep20, SweetDreams30 }), 
            new MindTopic("Falling asleep", new[] { FallingAsleep5, FallingAsleep10, FallingAsleep15, FallingAsleep20, FallingAsleep30 }), 
            new MindTopic("Sweet dreams", new[] { SweetDreams30 }), 
            new MindTopic("Sleeping problems", new[] { SleepingProblems60 })
        },
        "Before-sleep programs will make you feel tired. Use them in bed when you want to fall asleep.",
        "Assets/BeforeSleep.jpg"
        );

        #region ENERGIZING
        private static readonly FrequencyPoint[] Energizing15 = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 14, Time = 6.3},
                new FrequencyPoint{Frequency = 8, Time = 8.2},
                new FrequencyPoint{Frequency = 16, Time = 15}
            };
        private static readonly FrequencyPoint[] Energizing25 = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 13, Time = 5},
                new FrequencyPoint{Frequency = 8, Time = 10},
                new FrequencyPoint{Frequency = 14, Time = 15},
                new FrequencyPoint{Frequency = 8, Time = 20},
                new FrequencyPoint{Frequency = 17, Time = 25}
            };
        private static readonly FrequencyPoint[] Energizing35 =
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 13.5, Time = 5},
                new FrequencyPoint{Frequency = 8, Time = 10},
                new FrequencyPoint{Frequency = 14.5, Time = 15},
                new FrequencyPoint {Frequency = 8, Time = 20},
                new FrequencyPoint {Frequency = 14.5, Time = 25},
                new FrequencyPoint {Frequency = 8, Time = 30},
                new FrequencyPoint {Frequency = 17.5, Time = 35}
            };
        private static readonly FrequencyPoint[] Energizing45 = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 13.5, Time = 5},
                new FrequencyPoint{Frequency = 8, Time = 10},
                new FrequencyPoint{Frequency = 14.5, Time = 15},
                new FrequencyPoint{Frequency = 8, Time = 20},
                new FrequencyPoint{Frequency = 14.5, Time = 25},
                new FrequencyPoint{Frequency = 8, Time = 30},
                new FrequencyPoint{Frequency = 14.5, Time = 35},
                new FrequencyPoint{Frequency = 8, Time = 40},
                new FrequencyPoint{Frequency = 17.5, Time = 45}
            };
        private static readonly FrequencyPoint[] MindFocusing10 = 
            {
                new FrequencyPoint{Frequency = 25, Time = 0},
                new FrequencyPoint{Frequency = 6, Time = 0.5},
                new FrequencyPoint{Frequency = 25, Time = 1},
                new FrequencyPoint{Frequency = 6, Time = 1.5},
                new FrequencyPoint{Frequency = 20, Time = 2},
                new FrequencyPoint{Frequency = 6, Time = 2.5},
                new FrequencyPoint{Frequency = 20, Time = 3},
                new FrequencyPoint{Frequency = 6, Time = 3.5},
                new FrequencyPoint{Frequency = 20, Time = 4},
                new FrequencyPoint{Frequency = 6, Time = 5},
                new FrequencyPoint{Frequency = 20, Time = 5.5},
                new FrequencyPoint{Frequency = 18, Time = 6},
                new FrequencyPoint{Frequency = 18, Time = 7},
                new FrequencyPoint{Frequency = 10, Time = 7.5},
                new FrequencyPoint{Frequency = 2, Time = 8},
                new FrequencyPoint{Frequency = 11, Time = 8.5},
                new FrequencyPoint{Frequency = 11, Time = 9.5},
                new FrequencyPoint{Frequency = 14, Time = 10}
            };
        private static readonly FrequencyPoint[] WarmingUp22 = 
            {
                new FrequencyPoint{Frequency = 10, Time = 0},
                new FrequencyPoint{Frequency = 18, Time = 1},
                new FrequencyPoint{Frequency = 10, Time = 2},
                new FrequencyPoint{Frequency = 18, Time = 3},
                new FrequencyPoint{Frequency = 10, Time = 4},
                new FrequencyPoint{Frequency = 18, Time = 5},
                new FrequencyPoint{Frequency = 10, Time = 6},
                new FrequencyPoint{Frequency = 18, Time = 7},
                new FrequencyPoint{Frequency = 10, Time = 8},
                new FrequencyPoint{Frequency = 18, Time = 9},
                new FrequencyPoint{Frequency = 10, Time = 10},
                new FrequencyPoint{Frequency = 18, Time = 11},
                new FrequencyPoint{Frequency = 10, Time = 12},
                new FrequencyPoint{Frequency = 18, Time = 13},
                new FrequencyPoint{Frequency = 10, Time = 14},
                new FrequencyPoint{Frequency = 18, Time = 15},
                new FrequencyPoint{Frequency = 10, Time = 16},
                new FrequencyPoint{Frequency = 18, Time = 17},
                new FrequencyPoint{Frequency = 10, Time = 18},
                new FrequencyPoint{Frequency = 18, Time = 19},
                new FrequencyPoint{Frequency = 10, Time = 20},
                new FrequencyPoint{Frequency = 18, Time = 21},
                new FrequencyPoint{Frequency = 18, Time = 22}
            };
        private static readonly FrequencyPoint[] MaxPower30 = 
            {
                new FrequencyPoint{Frequency = 10, Time = 0},
                new FrequencyPoint{Frequency = 10, Time = 5},
                new FrequencyPoint{Frequency = 38, Time = 6},
                new FrequencyPoint{Frequency = 40, Time = 7},
                new FrequencyPoint{Frequency = 30, Time = 15},
                new FrequencyPoint{Frequency = 40, Time = 25},
                new FrequencyPoint{Frequency = 40, Time = 27},
                new FrequencyPoint{Frequency = 32, Time = 30}
            };
        #endregion ENERGIZING

        public static readonly MindCathegory Energizing = new MindCathegory("Energizing", new[]
        {
            new MindTopic("Short energizing", new[] { Energizing15 }), 
            new MindTopic("Classic energizing", new[] { Energizing25, Energizing35, Energizing45 }), 
            new MindTopic("Mind focusing", new[] { MindFocusing10 }), 
            new MindTopic("Warming up", new[] { WarmingUp22 }), 
            new MindTopic("Max power", new[] { MaxPower30 })
        },
        "Energizing programs will make you feel full of energy. Use them before a physical or mental activity or when you do not want to feel tired.",
        "Assets/Energizing.jpg", true);

        #region POWER_LEARNING
        private static readonly FrequencyPoint[] Learning15 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 4, Time = 7},
                new FrequencyPoint{Frequency = 6, Time = 10.5},
                new FrequencyPoint{Frequency = 12, Time = 15}
            };

        private static readonly FrequencyPoint[] Learning25 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 3, Time = 5},
                new FrequencyPoint{Frequency = 6, Time = 10.5},
                new FrequencyPoint{Frequency = 3, Time = 15},
                new FrequencyPoint{Frequency = 6, Time = 20.5},
                new FrequencyPoint{Frequency = 12, Time = 25}           
            };

        private static readonly FrequencyPoint[] Learning35 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 3, Time = 2.5},
                new FrequencyPoint{Frequency = 6, Time = 8},
                new FrequencyPoint{Frequency = 3, Time = 12.5},
                new FrequencyPoint{Frequency = 6, Time = 17.5},
                new FrequencyPoint{Frequency = 3, Time = 22.5},
                new FrequencyPoint{Frequency = 6, Time = 27.5},
                new FrequencyPoint{Frequency = 3, Time = 32.5},
                new FrequencyPoint{Frequency = 14, Time = 35}           
            };
        private static readonly FrequencyPoint[] Learning45 = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},
                new FrequencyPoint{Frequency = 3, Time = 2.5},
                new FrequencyPoint{Frequency = 6, Time = 8},
                new FrequencyPoint{Frequency = 3, Time = 12.5},
                new FrequencyPoint{Frequency = 6, Time = 17.5},
                new FrequencyPoint{Frequency = 3, Time = 22.5},
                new FrequencyPoint{Frequency = 6, Time = 27.5},
                new FrequencyPoint{Frequency = 3, Time = 32.5},
                new FrequencyPoint{Frequency = 6, Time = 37.5},
                new FrequencyPoint{Frequency = 3, Time = 42.5},                
                new FrequencyPoint{Frequency = 14, Time = 45}           
            };
        private static readonly FrequencyPoint[] ProblemSolving = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},
                new FrequencyPoint{Frequency = 8, Time = 1},
                new FrequencyPoint{Frequency = 4, Time = 3},
                new FrequencyPoint{Frequency = 4, Time = 9},
                new FrequencyPoint{Frequency = 11.5, Time = 11},
                new FrequencyPoint{Frequency = 12, Time = 15},
                new FrequencyPoint{Frequency = 12, Time = 18},
                new FrequencyPoint{Frequency = 8, Time = 19},
                new FrequencyPoint{Frequency = 8, Time = 20}
            };
        #endregion POWER_LEARNING

        public static readonly MindCathegory PowerLearning = new MindCathegory("Faster learning", new[] { 
            new MindTopic("Quick learning", new[] { Learning15 }, "Images/learn1.jpg"), 
            new MindTopic("Power learning", new[] { Learning25, Learning35, Learning45 }),
            new MindTopic("Language learning", new[] { Learning15, Learning25, Learning35, Learning45 }, "Images/learn3.jpg"),
            new MindTopic("Remember more", new[] { Learning35, Learning45 }, "Images/learn4.jpg"),
            new MindTopic("Problem solving", new[] { ProblemSolving })
        },
        "Learning programs will help you learn faster and remember more. Use them before learning activities or in combination with an audio book you want to learn from. ",
        "Assets/Learn.jpg", true);

        #region ENHANCE_BRAIN
        private static readonly FrequencyPoint[] BrainTuning = 
            {
                new FrequencyPoint{Frequency = 8, Time = 0},         
                new FrequencyPoint{Frequency = 8, Time = 4},         
                new FrequencyPoint{Frequency = 18, Time = 5},         
                new FrequencyPoint{Frequency = 18, Time = 8},         
                new FrequencyPoint{Frequency = 4, Time = 10},         
                new FrequencyPoint{Frequency = 4, Time = 12},         
                new FrequencyPoint{Frequency = 7, Time = 13.5},         
                new FrequencyPoint{Frequency = 8, Time = 15}
            };
        private static readonly FrequencyPoint[] BrainTraining = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 5, Time = 5},         
                new FrequencyPoint{Frequency = 12, Time = 10},         
                new FrequencyPoint{Frequency = 5, Time = 15},         
                new FrequencyPoint{Frequency = 12, Time = 20},         
                new FrequencyPoint{Frequency = 5, Time = 25},         
                new FrequencyPoint{Frequency = 12, Time = 30}
            };
        private static readonly FrequencyPoint[] CreativityEnhancing = 
            {
                new FrequencyPoint{Frequency = 16, Time = 0},         
                new FrequencyPoint{Frequency = 32, Time = 5},         
                new FrequencyPoint{Frequency = 33, Time = 8},         
                new FrequencyPoint{Frequency = 7, Time = 10},         
                new FrequencyPoint{Frequency = 16, Time = 15},         
                new FrequencyPoint{Frequency = 15, Time = 26},         
                new FrequencyPoint{Frequency = 15, Time = 29},         
                new FrequencyPoint{Frequency = 33, Time = 30}
            };
        private static readonly FrequencyPoint[] UnleashedCreativity = 
            {
                new FrequencyPoint{Frequency = 12, Time = 0},         
                new FrequencyPoint{Frequency = 12, Time = 2},         
                new FrequencyPoint{Frequency = 8, Time = 3},         
                new FrequencyPoint{Frequency = 8, Time = 7},         
                new FrequencyPoint{Frequency = 3, Time = 10},         
                new FrequencyPoint{Frequency = 3, Time = 14},         
                new FrequencyPoint{Frequency = 1, Time = 15},         
                new FrequencyPoint{Frequency = 1, Time = 18},               
                new FrequencyPoint{Frequency = 3, Time = 20},               
                new FrequencyPoint{Frequency = 3, Time = 24},               
                new FrequencyPoint{Frequency = 5, Time = 25},               
                new FrequencyPoint{Frequency = 5, Time = 30}
            };
        #endregion ENHANCE_BRAIN

        public static readonly MindCathegory EnhanceBrain = new MindCathegory("Enhance brain", new[] { 
            new MindTopic("Brain tuning", new[] { BrainTuning }), 
            new MindTopic("Brain training", new[] { BrainTraining }), 
            new MindTopic("Brain effectiveness", new[] { BrainTuning, BrainTraining, CreativityEnhancing, UnleashedCreativity }), 
            new MindTopic("Creativity enhancing", new[] { CreativityEnhancing }, "Images/learn2.jpg"),
            new MindTopic("Unleashed creativity", new[] { UnleashedCreativity })
        },
        "Brain-enhancing programs will help you enhance your brain capabilities and make you brain work more effectively.",
        "Assets/Creativity.jpg", true);

        public static readonly MindCathegory Addictions = new MindCathegory("Anti-addictions", new[] { 
            new MindTopic("Antismoke", new[] { Relaxation25, Relaxation35 }, "Images/anti1.jpg"), 
            new MindTopic("No alcohol", new[] { DeepRelaxation }), 
            new MindTopic("Diet support", new[] { DeepMeditation }), 
            new MindTopic("Anti gambling", new[] { ProgressiveMeditation }),
            new MindTopic("Internet addiction", new[] { DeepRelaxation, DeepMeditation, ProgressiveMeditation })
        },
        "Anti-addiction programs will support your determination to deal with an addiction.",
        "Assets/Addictions.jpg", true);

        public static readonly MindCathegory[] MindCathegories = new[]
        {
            Relaxation, 
            Meditation, 
            BeforeSleep, 
            Energizing, 
            PowerLearning, 
            EnhanceBrain,
            Addictions
        };
    }
}

