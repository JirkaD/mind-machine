﻿using System;
using System.Collections.Generic;
using MindMachineForWin8.Model;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace MindMachineForWin8.ViewModels
{
        public class FlashingViewModel : ViewModelBase
        {
            private Color _color;
            private DispatcherTimer _timer;
            private double _phase;
            private Brush _brush;
            private DateTime _timeOfPreviousTick;
            private TimeSpan _programRunningTime;
            private MindProgram _mindProgram;
            private String _music;

            public string Title { get; private set; }

            #region PROPERTIES
            public Color Color
            {
                get
                {
                    return _color;
                }
                set
                {
                    if (value != _color)
                    {
                        _color = value;
                        _brush = new SolidColorBrush(_color);
                        RaisePropertyChanged("Color");
                        RaisePropertyChanged("Brush");
                    }
                }
            }

            public Brush Brush
            {
                get { return _brush; }
            }

            public String TimeLeft
            {
                get
                {
                    var timeLeft = GetTimeLeft();
                    return FormatTimeStamp(timeLeft);
                }
            }

            private TimeSpan GetTimeLeft()
            {
                TimeSpan totalTime = _mindProgram.Lenght;
                TimeSpan timeLeft = totalTime - ProgramRunningTime;
                return timeLeft;
            }

            public static string FormatTimeStamp(TimeSpan timeLeft)
            {
                var hours = AddLeadingZeros(timeLeft.Hours, 2);
                var minutes = AddLeadingZeros(timeLeft.Minutes, 2);
                var seconds = AddLeadingZeros(timeLeft.Seconds, 2);

                const string separator = ":";
                return hours + separator + minutes + separator + seconds;
            }

            private static string AddLeadingZeros(int value, int numberOfZeroes)
            {
                string valueString = value.ToString();
                while (valueString.Length < numberOfZeroes)
                {
                    valueString = "0" + valueString;
                }
                return valueString;
            }

            private TimeSpan ProgramRunningTime
            {
                get { return _programRunningTime; }
                set
                {
                    var previousTime = GetTimeLeft();
                    _programRunningTime = value;
                    var afterChangeTime = GetTimeLeft();

                    if (previousTime.Seconds != afterChangeTime.Seconds)
                    {
                        RaisePropertyChanged("TimeLeft");
                    }
                }
            }

            #endregion PROPERTIES

            #region Tombstonig
            public void Activate(FlashingPageParameters fpp, Dictionary<string, object> pageState)
            {
                _color = Colors.Black;
                _brush = new SolidColorBrush(Colors.Black);
                _programRunningTime = new TimeSpan();
                _timeOfPreviousTick = new DateTime();
                _phase = 0;
                _mindProgram = fpp.MindProgram;
                _music = fpp.Music;

                if (_music != null)
                    MusicUri = new Uri("ms-appx:/Music/" + _music + ".wma");
                else
                    MusicUri = null;

                Title = fpp.TopicName;

/*                Object resumeAt;
                if (storage.TryGetValue("ResumeAt", out resumeAt))
                {
                    ProgramRunningTime = _mindProgram.Lenght - (TimeSpan)resumeAt;
                }*/

                CreateTimer();
            }

            private Uri _musicUri;
            public Uri MusicUri
            {
                get
                {
                    return _musicUri;
                }
                set
                {
                    _musicUri = value;
                    RaisePropertyChanged("MusicUri");
                }
            }

            private void CreateTimer()
            {
                _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(10) };
                _timer.Tick += OnTick;
                _timer.Start();
            }

          /*  public override void Deactivate(PhoneState storage)
            {
                var timeLeft = GetTimeLeft();
                storage.PageState["ResumeAt"] = timeLeft;
                storage.Transient["ElapsedTime"] = timeLeft;
                StopProgram();
            }*/
            #endregion Tombstoning

            void OnTick(object sender, object e)
            {
                var now = DateTime.Now;
                if (now - _timeOfPreviousTick < TimeSpan.FromSeconds(1))
                {
                    var frameSpan = now - _timeOfPreviousTick;
                    ProgramRunningTime += frameSpan;
                    var frequency = _mindProgram.GetFrequency(ProgramRunningTime);
                    if (frequency > 14)
                        frequency = 14;
                    _phase += 2 * Math.PI * frameSpan.TotalSeconds * frequency;

                    {
                        var tempColor = Math.Sin(_phase) < 0 ? Colors.Black : /*_settings.LightColor*/ Colors.White;
                        double visibility;
                        double secondsFromEnds = Math.Min(GetTimeLeft().TotalSeconds, _programRunningTime.TotalSeconds);
                        const double limit = 30;
                        if (secondsFromEnds >= limit)
                        {
                            visibility = 1;
                        }
                        else
                        {
                            visibility = secondsFromEnds / 30;
                        }
                        tempColor.R = (byte)(tempColor.R * visibility);
                        tempColor.G = (byte)(tempColor.G * visibility);
                        tempColor.B = (byte)(tempColor.B * visibility);
                        Color = tempColor;
                    }
                }
                _timeOfPreviousTick = now;


                if (GetTimeLeft() <= TimeSpan.Zero)
                {
                    _programRunningTime = _mindProgram.Lenght;
                    StopProgram();
                }
            }

            private void StopProgram()
            {
                _timer.Stop();
                Color = Colors.Black;
            }

        }
}